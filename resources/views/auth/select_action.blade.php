@extends('layouts.main')

@section('headerBlock')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">Select an action</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4 buttonsRegAccount">
                <a href="/register-account/create" class="btn btn-default btn-lg">Registration account</a>
                <hr>
                <a href="/register-account/add" class="btn btn-default btn-lg">I have an account</a>
        </div>
    </div>
@endsection