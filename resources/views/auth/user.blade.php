@extends('layouts.main')

@section('headerBlock')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">{{ucfirst(request()->role)}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">Authentication:
                        <a href="{{ url('/auth/google') }}" class="btn btn-success">Google G+</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>What is Lorem Ipsum?</h3>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries, but also the leap
                        into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem
                        Ipsum passages, and more recently with desktop publishing software like Aldus
                        PageMaker including versions of Lorem Ipsum.
                    </p>
                    <ul>
                        <li>Lorem ipsum</li>
                        <li>Lorem ipsum</li>
                    </ul>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum </p>
                </div>
            </div>
        </div>

        <!--START FORM-->
        <div class="col-md-6">
            <div class="panel panel-default title-main-form">
                <p class="boldFont">Searching files</p>
                <hr>
                <!--FORM-->
                <form class="form-horizontal main-form" action="" method="POST" ">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <div class="col-xs-10 col-sm-10 col-cd-6">
                        <label for="file">File:</label>
                        <input type="text" class="form-control input-md" name="file" id="file">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-10 col-sm-10 col-cd-6">
                        <label for="folder">Folder:</label>
                        <input type="text" class="form-control input-md" name="folder" id="folder">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-10 col-sm-10 col-cd-6">
                        <label for="moder">Moder:</label>
                        <input type="text" class="form-control input-md" name="moder" id="moder">
                    </div>
                </div>

                <!--button submit-->
                <div class="form-group">
                    <div class="col-xs-4">
                        {{--<input type="submit" class="btn btn-default" value="Submit" name="submit">--}}
                        <button class="btn btn-default">Search</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!--STOP FORM-->

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default title-main-form">
                <p class="boldFont">Result</p>
                <hr>
                @if(!empty($file))
                    <ul id="file">
                        <h4>Search results for: {{ $query }}</h4>
                        <img src="{{$file->thumbnail_link}}">
                        <br>
                        <a href="/pay-system">Buy</a>
                    </ul>
                @else
                    <p>Not results</p>
                @endif
            </div>
        </div>
    </div>

    {{--<h3>Searching files</h3>--}}
    {{--<form method="POST">--}}
        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

        {{--<div class="form-group">--}}
            {{--<label for="file_name">File title</label>--}}
            {{--<input type="text" name="file_name" id="file_name">--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<label for="folder_title">Folder title</label>--}}
            {{--<input type="text" name="folder_title" id="folder_title">--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<label for="moder_email">Moder email</label>--}}
            {{--<input type="text" name="moder_email" id="moder_email">--}}
        {{--</div>--}}

        {{--<button class="button-primary">Search</button>--}}
    {{--</form>--}}

    {{--@if(!empty($file))--}}
        {{--<ul id="file">--}}
            {{--<h4>Search results for: {{ $query }}</h4>--}}
            {{--<img src="{{$file->thumbnail_link}}">--}}
            {{--<br>--}}
            {{--<a href="/pay-system">Buy</a>--}}
        {{--</ul>--}}
    {{--@else--}}
        {{--<p>Not results</p>--}}
    {{--@endif--}}
@endsection