@extends('layouts.main')

@section('headerBlock')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">Gallery uploads</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2 galleryUploads">
            <a href="/home/moder/gallery?interval=0" class="btn btn-default btn-lg">Today</a>
            <a href="/home/moder/gallery?interval=1" class="btn btn-default btn-lg">Yesterday</a>
            <a href="/home/moder/gallery?interval=2" class="btn btn-default btn-lg">3 days ago</a>
            <a href="/home/moder/gallery?interval=6" class="btn btn-default btn-lg">Week</a>
            <a href="/home/moder/gallery?interval=30" class="btn btn-default btn-lg">Mount</a>
            <a href="/home/moder/gallery?interval=182" class="btn btn-default btn-lg">6 months</a>
            <a href="/home/moder/gallery?interval=365" class="btn btn-default btn-lg">Year</a>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
                <div class="panel-body">
                    @if(!($uploads->isEmpty()))
                        @foreach($uploads as $upload)
                            <div class="col-md-12">
                                <hr>
                                <div class="col-md-3">
                                    <img src="{{$upload->content_link}}" alt="" width="250" height="200" class="img-thumbnail">
                                </div>
                                <div class="col-md-4">
                                    <p><b>ID</b>: {{$upload->google_drive_id}}</p>
                                    <p><b>Mime type:</b> {{$upload->mime_type}}</p>
                                    <p><b>File uploaded time</b>: {{$upload->created_at}}</p>
                                    <p><b>Size</b>: {{ round ($upload->size/1024/1024,3)}} MB</p>

                                </div>
                                <div class="col-md-3">
                                    <p><b>Title file</b>: {{$upload->title}}</p>
                                    <p><b>Title dir</b>: {{$upload->parent_dir_title}}</p>
                                    <p>--</p>
                                    <p><b>View link</b>: <a href="{{$upload->view_link}}">google drive</a></p>
                                </div>
                                <div class="col-md-2">
                                    <p><b>Description</b>: {{ ($upload->description) ?? 'no description' }}</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <code>Not result</code>
                    @endif
            </div>
        </div>
    </div>
@endsection