@extends('layouts.main')

@section('headerBlock')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">{{ucfirst(Auth::user()->role)}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <!--NAVIGATION START-->
        <div class="col-md-3">
            <nav class="navmenu navmenu-default accountRightNavMenu" role="navigation">
                <hr>
                <p class="navmenu-brand" href="">Actions</p>
                <hr>
                <ul class="nav navmenu-nav">
                    <li class="btn btn-default"><a href="/home/moder/upload">Upload</a></li>
                    <li class="btn btn-default"><a href="/home/moder/gallery">Gallery</a></li>
                    <li class="btn btn-default"><a href="#">Payment systems</a></li>
                    {{--<li class="dropdown btn btn-default">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Пункт 3 <b class="caret"></b></a>--}}
                        {{--<ul class="dropdown-menu navmenu-nav" role="menu">--}}
                            {{--<li><a href="#">Пункт 3.1</a></li>--}}
                            {{--<li><a href="#">Пункт 3.2</a></li>--}}
                            {{--<li><a href="#">Пункт 3.3</a></li>--}}
                            {{--<li><a href="#">Пункт 3.4</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </nav>
        </div>
        <!--NAVIGATION STOP-->

        <!--EMPTY START-->
        <div class="col-md-1"></div>
        <!--EMPTY STOP--

        <!--ACTIVITY START-->
        <div class="col-md-4"> <!--col-md-offset-2-->
            <hr>
                <p class="accountRightNavMenu" href="">Activity</p>
            <hr>
            <div class="panel panel-default">
                <p class="accountActivity">You registered: <i>{{$account['getDataReg']}}</i></p>
                <p class="accountActivity">Sum uploads: <i>{{$account['getSumUploads']}} file(s)</i></p>

                <hr>
                <p><code>---</code></p>
                <p><code>---</code></p>
                <p><code>---</code></p>
                <p><code>Payment system</code></p>
                <p><code>---</code></p>
                <p><code>---</code></p>
                <p><code>---</code></p>

            </div>
        </div>
        <!--ACTIVITY STOP-->

        <!--EMPTY START-->
        <div class="col-md-1"></div>
        <!--EMPTY STOP-->

        <div class="col-md-3">
            <hr>
            <p class="accountRightNavMenu" href="">List emails</p>
            <hr>
            <div class="panel panel-default">
                @foreach($account['listEmails'] as $email)
                    <p class="accountActivity">{{$email}}</p>
                @endforeach
            </div>
        </div>

    </div>
@endsection