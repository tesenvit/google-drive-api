@extends('layouts.main')

@section('headerBlock')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="boldFont">Upload</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <!----------------->
        <!-- LEFT START  -->
        <!----------------->
        <div class="col-md-5">

            <!---FORM START -->
                <div class="panel panel-default">
                    <div class="panel-heading">Upload file</div>
                    <div class="panel-body">
                        <hr>
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="file" class="col-md-3 control-label">Select file:</label>
                                <div style="position:relative">
                                    <a class='btn btn-default' href='javascript:' style="margin-left: 15px">
                                        Choose File...
                                        <input type="file" style='position:absolute;
                                                            z-index:2;
                                                            top:0;
                                                            left:0;
                                                            filter: alpha(opacity=0);
                                                            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
                                                            opacity:0;
                                                            background-color:transparent;
                                                            color:transparent;'
                                               name="file"
                                               id="file"
                                               size="60"
                                               onchange='$("#upload-file-info").html($(this).val());'>
                                    </a>
                                    <span class='label label-default' id="upload-file-info"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="desc" class="col-md-3 control-label">Description:</label>
                                <div class="col-md-8">
                                    <textarea id="desc" class="form-control" name="desc"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <button class="btn btn-primary" style="margin-bottom: 6px;">
                                        Upload
                                    </button>
                                </div>
                            </div>

                        </form>
                        <hr>
                    </div>
                </div>
            <!--STOP FORM-->

        </div>
        <!----------------->
        <!-- LEFT STOP  -->
        <!----------------->


        <!----------------->
        <!-- RIGHT START -->
        <!----------------->
        <div class="col-md-7">

            <div class="panel panel-default">
                <div class="panel-heading">Uploaded file</div>
                <div class="panel-body">
                    @if(session('message.type')=='success')
                        <code style="text-align: center">{{session('message.text')}}</code>
                        <hr>
                        <div class="col-md-6">
                            <img src="{{$lastUploadFile->content_link}}" width="240" height="240" class="img-thumbnail">
                        </div>
                        <div class="col-md-6">
                            <p><b>ID</b>: {{$lastUploadFile->google_drive_id}}</p>
                            <p><b>Mime type</b>: {{$lastUploadFile->mime_type}}</p>
                            <p><b>Size</b>: {{ round ($lastUploadFile->size/1024/1024,3)}} MB</p>
                            <p><b>Title file</b>: {{$lastUploadFile->title}}</p>
                            <p><b>Title folder</b>: {{$lastUploadFile->parent_dir_title}}</p>
                            <p><b>Description</b>: {{ ($lastUploadFile->description) ?? 'no description' }}</p>
                        </div>
                    @else
                        <code>Not result. Upload file</code>
                    @endif
                </div>
            </div>

        </div>
        <!----------------->
        <!-- RIGHT STOP -->
        <!----------------->

    </div>
@endsection