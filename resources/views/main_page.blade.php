@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-5"></div>

        <div class="col-xs-4">
            <p class="role-main">Role</p>

            <div>
                <a href="/login?role=moder" class="btn btn-danger btn-lg">Moder</a>
                <a href="/login?role=user" class="btn btn-success btn-lg">User</a>
            </div>
        </div>
    </div>
@endsection
