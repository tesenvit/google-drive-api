<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <title>New beginning</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>

<body>
<!--START NAVIGATION-->
<div class="container">
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <!--"sandwich" for mobile device-->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/" class="navbar-brand">New beginning</a>
        </div>
            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    {{--@if (Route::has('login'))--}}
                        {{--@if (Auth::check())--}}
                            {{--<li class=""><a href="{{ url('/home') }}">Home</a></li>--}}
                            {{--<li class=""><a href="{{ route('logout') }}">Logout</a></li>--}}
                        {{--@else--}}
                            {{--<li class=""><a href="{{ route('login') }}">Login</a></li>--}}
                            {{--<li class=""><a href="{{ route('register') }}">Register</a></li>--}}
                        {{--@endif--}}
                    {{--@endif--}}
                    @if (Auth::guest())
                        {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
                        {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                @if(Auth::user()->account()->first())
                                    {{Auth::user()->account()->first()->name}}
                                @endif
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                <li>
                                    <a href="/home/moder/gallery">Gallery</a>
                                </li>
                                <li>
                                    <a href="/home/moder/upload">Upload</a>
                                </li>
                                <br>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
        </div>
    </nav>
</div>
<!--STOP NAVIGATION-->

<div class="content">
    <!--START HEADER-->
    <div class="container">
        @yield('headerBlock')
    </div>
    <!--STOP HEADER-->

    <!--START CONTENT-->
    <div class="container">
        @yield('content')
    </div>
    <!--STOP CONTENT-->
</div>


<!--START FOOTER-->
<div class="container">
    <div class="">
        <div class="navbar navbar-default" id="navbar">
            <span class="footerTextLeft">{{date('j F Y')}}</span>
            <p class="footerTextRight">Desing by&nbsp;&nbsp;<a href="#">balaev INC</a></p>
        </div>
    </div>
</div>
<!--STOP FOOTER-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>
</html>