<?php

namespace App\Http\Middleware;

use Closure;
use App\Account;
use Illuminate\Support\Facades\Auth;

class CheckAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accountId = Auth::user()->account_id;
        if(!$accountId){
            return redirect('/register-account');
        }

        return $next($request);
    }
}
