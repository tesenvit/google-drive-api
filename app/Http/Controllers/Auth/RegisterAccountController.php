<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class RegisterAccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('auth.select_action');
    }

    public function showRegistrationAccountForm()
    {
        return view('auth.register_account');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:100|unique:accounts',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function createAccount(Request $request)
    {
        $this->validator($request->all())->validate();

        $account = Account::create([
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'active' => true
        ]);

        $user = User::find(Auth::user()->id);
        $user->account_id = $account->id;
        $user->save();

        return redirect('home');
    }

    public function showAddAccountForm()
    {
        return view('auth.add_to_account');
    }


    public function addToAccount(Request $request)
    {
        $name = $request->name;
        $pass = $request->password;
        $account = Account::where('name',$name)->first();

        if (Hash::check($pass, $account->password)) {
            $user = User::find(Auth::user()->id);
            $user->account_id = $account->id;
            $user->save();
        }

        return redirect('home');
    }
}
