<?php

namespace App\Http\Controllers;

use App\Upload;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainPageController extends Controller
{
    public function index()
    {
        return view('main_page');
    }
}
