<?php

namespace App\Http\Controllers\Account;

use App\Account;
use App\User;
use App\GoogleDrive;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\CheckAccount;
use App\Http\Controllers\GoogleDriveController;

class AccountController extends Controller
{
    protected $titleApp = 'new-beginning';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkAccount');
    }

    public function index()
    {
        $role = Auth::user()->role;
        return redirect('/home/'.$role);
    }

    public function googleDrive()
    {
        $googleClient = GoogleDrive::client();
        $googleDrive = GoogleDrive::drive($googleClient);
        return $googleDrive;
    }

    public function getDateOfRegistration()
    {
        $dateRegAcc = Auth::user()->account()->first()->created_at;
        $created = new Carbon($dateRegAcc);
        $now = Carbon::now('Europe/Kiev');

        return $created->diffForHumans($now);
    }

    public function getAccountId()
    {
        $id = Auth::user()->account()->first()->id;
        return $id;
    }

}
