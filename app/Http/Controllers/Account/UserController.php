<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends AccountController
{
    public function index()
    {
        return view('account.user.index');
    }
}
