<?php

namespace App\Http\Controllers\Account;

use App\User;
use App\Upload;
use App\Account;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ModerController extends AccountController
{
    public function index()
    {
        $this->getListEmails();

        $account = [
            'getDataReg' => $this->getDateOfRegistration(),
            'getSumUploads' => $this->getSumUploads(),
            'listEmails' => $this->getListEmails(),
        ];

        return view('account.moder.index',compact('account'));
    }

    public function gallery(Request $request)
    {
        if($request->has('interval')){
            if($request->interval * 1 || $request->interval == '0'){  // is number?
                $fromDate = Carbon::now('Europe/Kiev')
                                  ->subDays($request->interval)
                                  ->toDateString();

                $uploads = Upload::where('account_id','=', $this->getAccountId())
                                 ->where( 'created_at', '>=', $fromDate)
                                 ->orderBy('created_at', 'desc')
                                 ->get();


//                foreach ($uploads as $upload){
//                    $img = Image::make($upload->content_link)->resize(150, 110);
//                    $mime = $upload->mime_type;
//                    $img->encode($mime);
//                    $base64 = 'data:' . $mime . ';base64,' . base64_encode($img);
//                    $images[] = $base64;
//                }
//
//                dd($images);

            }
        }else{
            return redirect('/home/moder/gallery?interval=0');
        }

        return view('account.moder.gallery',compact('uploads'));
    }

//    public function getThumbnail($resource,$height,$width)
//    {
//        $img = Image::make($resource)->resize($height, $width);
//        $img->encode('image/jpeg');
//        $type = 'image/jpeg';
//        $base64 = 'data:' . $type . ';base64,' . base64_encode($img);
//    }

    public function getIdFolder($nameFolder)
    {
        $parameters = [
            'q' => "mimeType = 'application/vnd.google-apps.folder'
                                and name = '$nameFolder'
                                and trashed = false"
        ];

        $result = $this->googleDrive()->files->listFiles($parameters);

        if($result->files){
            $id = $result->files[0]->id;
        }else {
            $fileMetaData = new Google_Service_Drive_DriveFile([
                'name' => $nameFolder,
                'mimeType' => 'application/vnd.google-apps.folder']);

            $file = $this->googleDrive()->files->create($fileMetaData, [
                'fields' => 'id']);

            $id = $file->getId();
        }

        return $id;
    }

    public function getDayFolderId()
    {
        // create unique name folder
        $name = Auth::user()->account()->first()->name;
        $date =  Carbon::now('Europe/Kiev')->toDateString();
        $uniqueName = $date.'_'.$name;

        //create main folder if not exist
        $idMain = $this->getIdFolder($this->titleApp);

        //create unique folder for each day
        $idToday = $this->getIdFolder($uniqueName);

        $emptyFileMetadata = new Google_Service_Drive_DriveFile();

        // Retrieve the existing parents to remove
        $folder = $this->googleDrive()->files->get($idToday, ['fields' => 'parents']);

        $previousParents = join(',', $folder->parents);

        // Move the folder to the new folder
        $this->googleDrive()->files->update($idToday, $emptyFileMetadata, [
            'addParents' => $idMain,
            'removeParents' => $previousParents,
            'fields' => 'id, parents']);

        return $idToday;
    }

    public function getListEmails()
    {
        $users = User::where('account_id','=',$this->getAccountId())->get();

        $listEmails = [];
        foreach ($users as $user){
            $listEmails[] = $user->email;
        }

        return $listEmails;
    }

    /**
     * Uploads files
     */
    public function upload()
    {
        $lastUploadFile = $this->getLastUploadedFile();
        return view('account.moder.upload',compact('lastUploadFile'));
    }

    public function doUpload(Request $request)
    {
        if ($request->hasFile('file')) {

            //build file
            $file = $request->file('file');
            $mimeType = $file->getMimeType();
            $title = $file->getClientOriginalName();
            $description = $request->input('desc');

            //convert to googleDrive file
            $driveFile = new Google_Service_Drive_DriveFile([
                'name' => $title,
                'mimeType' => $mimeType,
                'description' => $description,
                'parents' => [$this->getDayFolderId()],
                //'parents' => ['appDataFolder']
            ]);

            //type 'string'
            $data = file_get_contents($file);

            try {
                //create new file in googleDrive
                $createdFile = $this->googleDrive()->files->create($driveFile, [
                    'uploadType' => 'multipart',
                    'data' => $data,
                    'mimeType' => $mimeType,
                ]);

                $driveFileId = $createdFile->getId();

                // grant file permissions
                $userPermission = new Google_Service_Drive_Permission([
                    'type' => 'anyone',
                    'role' => 'reader',
                    // 'allowFileDiscovery' => false
                ]);
                $this->googleDrive()->permissions->create(
                    $driveFileId, $userPermission, ['fields' => 'id']);
                ///////////////////////////////////////////////

                // get new file
                $newFile = $this->googleDrive()->files->get($driveFileId,[
                    'fields' => 'id, name, iconLink, webViewLink, webContentLink, thumbnailLink, hasThumbnail, parents, size, mimeType'
                ]);
                ///////////////////////////////////////////////

                // get parent dir title
                $fileParent = $this->googleDrive()->files->get($newFile->getParents()[0],[
                    'fields' => 'name'
                ]);
                $titleParentDir = $fileParent->getName();
                ///////////////////////////////////////////////



//////////////////// Store in DataBase //////////////////////////////
                $upload = new Upload;
                $upload->google_drive_id = $driveFileId;
                $upload->description = $description;
                $upload->mime_type = $newFile->getMimeType();
                $upload->size = $newFile->getSize();
                $upload->title = $newFile->getName();
                $upload->thumbnail_link = $newFile->getThumbnailLink() ?? '0'; //PHP 7
                $upload->content_link = $newFile->getWebContentLink();
                $upload->view_link = $newFile->getWebViewLink();
                $upload->parent_dir_id = $newFile->getParents()[0];
                $upload->parent_dir_title = $titleParentDir;
                $upload->account_id = Auth::user()->account_id;

                $upload->save();
////////////////////////////////////////////////////////////////////////////


                return redirect('/home/moder/upload')
                    ->with('message', [
                        'type' => 'success',
                        'text' => "File was uploaded. ID: {$driveFileId}"
                    ]);

            } catch (Exception $e) {

                return redirect('/home/moder/upload')
                    ->with('message', [
                        'type' => 'error',
                        'text' => 'An error occurred while trying to upload the file'
                    ]);
            }
        }else {
            return redirect('/home/moder/upload');
        }
    }

    public function getSumUploads()
    {
        $amount = Upload::where('account_id', '=', $this->getAccountId())->count();
        return $amount;
    }

//    public function getUploads()
//    {
//        $uploads = Upload::where('account_id', '=', $this->getAccountId())->get();
//        return $uploads;
//    }

    public function getLastUploadedFile()
    {
        $lastUploadFile = Upload::where('account_id','=',$this->getAccountId())
            ->get()
            ->last();

        return $lastUploadFile;
    }

}
