<?php

namespace App;

use Google_Client;
use Google_Service_Drive;
use Illuminate\Database\Eloquent\Model;

class GoogleDrive extends Model
{
    public static function client()
    {
//        $googleClientToken = [
//            'access_token' => session('user.accessToken'),
//            'refresh_token' => session('user.refreshToken'),
//            'expires_in' => session('user.expiresIn')
//        ];


//        $client = new Google_Client();
//        $client->setApplicationName("new-beginning");
//        $client->setDeveloperKey(json_encode(env('GOOGLE_SERVER_KEY')));
//        $client->setAccessToken(json_encode($googleClientToken));
//        $client->setAccessType('offline');
//        $client->refreshToken(json_encode($googleClientToken['refresh_token']));

        $client = new Google_Client();
        $client->setApplicationName('new-beginning');
        $client->setScopes([Google_Service_Drive::DRIVE]);
        $client->setClientId(env('GOOGLE_ID'));
        $client->setClientSecret(env('GOOGLE_SECRET'));
        $client->setRedirectUri(env('GOOGLE_URL'));
        $client->setAccessType('offline');
        $client->setDeveloperKey(env('GOOGLE_SERVER_KEY'));
        $client->setAccessToken(session('user.accessToken'));

        return $client;
    }

    public static function drive($client)
    {
        $drive = new Google_Service_Drive($client);
        return $drive;
    }
}
