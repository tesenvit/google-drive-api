<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable = ['google_drive_id, description, mime_type, size, title, thumbnail_link, content_link, view_link, parent_dir_id, parent_dir_title, account_id'];

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

}
