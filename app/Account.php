<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name', 'password', 'active'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }
}
