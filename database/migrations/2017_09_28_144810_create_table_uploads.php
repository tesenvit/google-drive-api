<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_drive_id');
            $table->string('description')->nullable();
            $table->string('mime_type');
            $table->string('size');
            $table->string('title');
            $table->string('thumbnail_link')->nullable();
            $table->string('content_link');
            $table->string('view_link')->nullable();
            $table->string('parent_dir_id');
            $table->string('parent_dir_title');
            $table->string('account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
