<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| MAIN PAGE
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::get('/','MainPageController@index');
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////


/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| AUTHENTICATION
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);

Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');

Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////


/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| REGISTRATION ACCOUNT
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::get('/register-account', [
    'as' => 'register-account',
    'uses' => 'Auth\RegisterAccountController@index'
]);

// create account
Route::get('/register-account/create', [
    'as' => 'register-account.create',
    'uses' => 'Auth\RegisterAccountController@showRegistrationAccountForm'
]);

Route::post('/register-account/create', [
    'as' => '',
    'uses' => 'Auth\RegisterAccountController@createAccount'
]);

// add to account
Route::get('/register-account/add', [
    'as' => 'register-account.add',
    'uses' => 'Auth\RegisterAccountController@showAddAccountForm'
]);

Route::post('/register-account/add', [
    'as' => '',
    'uses' => 'Auth\RegisterAccountController@addToAccount'
]);
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////


/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| HOME (redirect to moder account or user account)
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::get('/home', [
    'as' => 'home',
    'uses' => 'Account\AccountController@index'
]);
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////


/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| MODERATOR
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::group(['middleware' => ['checkModer']],function(){

    Route::get('/home/moder','Account\ModerController@index');

    // Upload
    Route::get('/home/moder/upload','Account\ModerController@upload');

    Route::post('/home/moder/upload','Account\ModerController@doUpload');

    // Gallery
    Route::get('/home/moder/gallery','Account\ModerController@gallery');

});
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////


/*/////////////////////////////////////////////////////////////////////////
|--------------------------------------------------------------------------
| USER
|--------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////*/
Route::group(['middleware' => ['checkUser']],function(){

    Route::get('/home/user','Account\UserController@index');
});
//-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////